[![pipeline status](https://gitlab.com/XenGi-containers/archlinux-aur/badges/master/pipeline.svg)](https://gitlab.com/XenGi-containers/archlinux-aur/commits/master)

# archlinux-aur

archlinux with aur helper (yay) preinstalled

## Usage in Dockerfile

```
RUN yay -Syyu --noconfirm --noprogressbar some-package
```
